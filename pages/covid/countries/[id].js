import React from 'react';
import Banner from '../../../components/Banner';
import DoughnutChart from '../../../components/DoughnutChart';
import toNum from '../../../helpers/toNum';

export default function index({ country }) {
	return (
		<React.Fragment>
			<Banner 
				country={country.country_name}
				deaths={country.deaths}
				criticals={country.serious_critical}
				recoveries={country.total_recovered}
			/>
			<DoughnutChart 
				criticals={toNum(country.serious_critical)}
				deaths={toNum(country.deaths)}
				recoveries={toNum(country.total_recovered)}
			/>
		</React.Fragment>
	)
}

export async function getStaticPaths(){

	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})

	const data = await res.json();

	const paths = data.countries_stat.map(country => ({
		params: { id: country.country_name }
	}))

	// ["/covid/countries/USA", "/covid/countries/India", "/covid/countries/Philippines"]

	// /covid/countries/hfawhfh2h2

	return { paths, fallback: false }

}

// props {
// 	params: {
// 		id: USA
// 	}
// }

// "/covid/countries/USA"
// "/covid/countries/India"

// /covid/countries/USA

// /covid/countries/:id

// /courses/:id

export async function getStaticProps({params}) {

	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})

	const data = await res.json();

	const country = data.countries_stat.find(country => country.country_name === params.id)

	return {
		props: {
			country
		}
	}

}

// "/countries/covid/:id"