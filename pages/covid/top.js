import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import toNum from '../../helpers/toNum';

export default function Top({data}) {

	console.log(data);
	const countriesStats = data.countries_stat;

	const countriesCases = countriesStats.map(country => {
		return {
			name: country.country_name,
			cases: toNum(country.cases)
		}
	})

	console.log(countriesCases);

	const countries = countriesCases.slice(0, 10)

	console.log(countries);

	const cases = countries.map(country => {
		return country.cases
	})

	console.log(cases);

	const labels = countries.map(country => {
		return country.name
	})

	console.log(labels);

	return(
		<React.Fragment>
			<h1>Top 10 Countries With The Highest Number of Cases</h1>
			<Doughnut 
				data={{
					datasets: [{
						data: cases,
						backgroundColor: ["red", "orange", "yellow", "blue", "green", "indigo", "violet", "black", "gray", "salmon"]
					}],
					labels: labels
				}}
				redraw={false}
			/>
		</React.Fragment>
	)
}

export async function getStaticProps() {

	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})

	const data = await res.json();

	return {
		props: {
			data
		}
	}

}