export default function toNum(str) {

	// Converts the string to an array and to also gain access to array methods
	const arr = [...str];
	/*"6,846,010"
	["6", ",", "8", "4", "6", ",", "0", "1", "0"]*/

	// Filter our the commas in the string
	const filteredArr = arr.filter(element => element !== ',');
	// filteredArr = ["6", "8", "4", "6", "0", "1", "0"]

	// Reduce the filtered array back to a single string without commas
	return parseInt(filteredArr.reduce((x, y) => x + y));
	// "6846010" => 6846010

}

// 6846010
// string = String(6846010) = "6846010"
// arr = [...string]
// ["6", "8", "4", "6", "0", "1", "0"]
// for(i=arr.length; i > 0; i--){
// 	if(arr.length -3){
// 		x = arr.slice(arr.length-3)
// 		y= arr.slice(arr.length-3)
// 		arr.push(',')
// 		x + y
// 	}
// }